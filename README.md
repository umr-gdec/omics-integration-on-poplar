This git repository is associated to the publication Mardoc et al., Genomic data integration tutorial, a plant case study, 2024 (https://doi.org/10.1186/s12864-023-09833-0).

#### Data import

1) Download data in data.gouv.fr (https://entrepot.recherche.data.gouv.fr/privateurl.xhtml?token=d946bb29-4698-4bee-9c6b-c2d98558ca8a)
2) Store the three matrices in "data" folder

#### Source files

begin_analysis.R -> a generic code to run basic preliminary analysis and cimDiablo_v2 on datasets in the required format

omics_integration_on_poplar.Rmd -> case study of the article's workflow and cimDiablo_v2 on poplar data using more complex and less adaptive functions. 

cimDiablo_v2.R -> a new function presented in the article and applied on poplar data, but which can also be used on other datasets



#### R and libraries versions

All the code was run on R 4.2.1 with RStudio 2023.3.0.386.

Used libraries :

|   Package    |  Version  |                  Install                     |
|:-------------|:----------|:---------------------------------------------|
| tidyverse    | 1.3.2     | install.packages("tidyverse")                |
| mixOmics     | 6.20.0    | BiocManager::install('mixOmics')             |
| corrplot     | 0.92      | install.packages('corrplot')                 |
| data.table   | 1.14.2    | install.packages("data.table")               |
| mrdwabmisc   | 0.3.0     | remotes::install_github("mrdwab/mrdwabmisc") |
| gplots       | 3.1.3     | install.packages('gplots')                   |
| treemap      | 2.4-4     | install.packages("treemap")                  |
| RColorBrewer | 1.1-3     | install.packages("RColorBrewer")             |
| ggh4x        | 0.2.3     | install.packages("ggh4x")                    |


#### Known issues, limitations

Especially for running the PCA on the poplar data, the code needs important computer memory (16Go was not enough, but 32Go was in our tests)

Hence, we also make available the pdf and html in case you have trouble running all the code: omics_integration_on_poplar.pdf or omics_integration_on_poplar.html